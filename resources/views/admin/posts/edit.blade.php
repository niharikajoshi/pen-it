@extends('admin.layouts.app')

@section('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Post</h6>
                </div>
                <div class="card-body">
                    <form action="{{ route('posts.update', $post) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text"
                                   name="title"
                                   value="{{ $post->title }}"
                                   class="form-control"
                                   id="title"
                                   placeholder="Enter Post Title">
                        </div>
                        <div class="form-group">
                            <label for="excerpt">Excerpt</label>
                            <input type="text"
                                   name="excerpt"
                                   value="{{ $post->excerpt }}"
                                   class="form-control"
                                   id="excerpt">
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea class="form-control" id="body" name="body">{{ $post->body }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select id="category" class="form-control select2">
                                @foreach($categories as $category)
                                    @if($post->category_id === $category->id)
                                        <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <select name="tags[]" id="tags" class="form-control select2" multiple>
                                @foreach($tags as $tag)
                                        <option value="{{ $tag->id }}"
                                                {{ $post->hasTag($tag->id) ? 'selected' : '' }}
                                        >
                                            {{ $tag->name }}
                                        </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="published_at">Published At</label>
                            <input type="date"
                                   class="form-control"
                                   name="published_at"
                                   id="published_at">
                        </div>
                        <div class="form-group">
                            <label for="image">Image</label>
                            <input type="file" class="form-control" id="image" name="image">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('.select2').select2({
            placeholder: 'Select an option',
            allowClear: true
        });

    </script>
    <script>
        let defaultDate = "{{ $post->published_date_string }}";
        console.log(defaultDate);
        document.getElementById('published_at').defaultValue = defaultDate;
    </script>
@endsection
