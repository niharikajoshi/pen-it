<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['verifyCategoryCount'])->only('create');
        $this->middleware(['validatePostAuthor'])->only('edit', 'update', 'destroy');
    }

    public function index()
    {
        $posts = Post::with('category')->get();
        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.create', compact(['tags', 'categories']));
    }

    public function store(Request $request)
    {
        $image = $request->file('image')->store('posts');
        // Remember to run php artisan storage:link
        // and also in .env make FILESYSTEM_DISK as public

        $post = Post::create([
            'title' => $request->title,
            'excerpt' => $request->excerpt,
            'body' => $request->body,
            'published_at' => $request->published_at,
            'category_id' => $request->category_id,
            'user_id' => auth()->id(), // TODO: to be replaced with user id for logged in user!
            'image' => $image,
        ]);
        // Used to enter in many to many table
        $post->tags()->attach($request->tags);
        return redirect(route('posts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Post added successfully!'
            ]);
    }

    public function show(Post $post)
    {
        //
    }

   public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit', compact([
            'tags',
            'categories',
            'post'
        ]));
    }

    public function update(Request $request, Post $post)
    {
        $data = $request->only(['title', 'excerpt', 'body', 'published_at', 'category_id']);

        if($request->hasFile('image')) {
            $image = $request->file('image')->store('posts');
            $data['image'] = $image;
            $post->deleteImage();
        }
        $post->update($data);

        $post->tags()->sync($request->tags);

        return redirect(route('posts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Post Updated successfully!'
            ]);
    }
    public function trashed() {
        $posts = Post::onlyTrashed()->get();
        return view('admin.posts.trashed', compact('posts'));
    }
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect(route('posts.index'))
            ->with([
                'status' => 'success',
                'message' => 'Post Deleted Successfully'
            ]);


    }
}

