<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index() {
        $categories = Category::all();
        $tags = Tag::all();
        $posts = Post::published()->search()->simplePaginate(2);
        return view('frontend.index', compact(['categories', 'tags', 'posts']));
    }

    public function tags(Tag $tag) {
        $posts = $tag->posts()->published()->search()->simplePaginate(2);
        $categories = Category::all();
        $tags = Tag::all();
        return view('frontend.index', compact(['categories', 'tags', 'posts']));
    }

    public function category(Category $category) {
        $posts = $category->posts()->published()->search()->simplePaginate(2);
        $categories = Category::all();
        $tags = Tag::all();
        return view('frontend.index', compact(['categories', 'tags', 'posts']));
    }
}
