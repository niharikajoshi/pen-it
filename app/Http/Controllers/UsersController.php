<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index() {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function makeAdmin(User $user) {
        $user->update(['role' => 'admin']);
        session()->flash('status', 'success');
        session()->flash('message', 'User upgraded as admin!');
        return redirect(route('users.index'));
    }
}
