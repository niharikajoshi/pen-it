<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tags.index', compact(['tags']));
    }

    public function create()
    {
        return view('admin.tags.create');
    }

    public function store(Request $request)
    {
        Tag::create(['name' => $request->name]);
        session()->flash('status', 'success');
        session()->flash('message', 'Tag added successfully!');
        return redirect(route('tags.index'));
    }

    public function edit(Tag $tag)
    {
        return view('admin.tags.edit', compact(['tag']));
    }

    public function update(Request $request, Tag $tag)
    {
        $tag->update(['name' => $request->name]);
        session()->flash('status', 'success');
        session()->flash('message', 'Tag updated successfully!');
        return redirect(route('tags.index'));
    }

    public function destroy(Tag $tag)
    {
        if($tag->posts()->count() > 0) {
            session()->flash('status', 'danger');
            session()->flash('message', 'Tag has Posts! Cannot Delete');
            return redirect(route('tags.index'));
        }

        $tag->delete();
        session()->flash('status', 'success');
        session()->flash('message', 'Tag deleted successfully!');
        return redirect(route('tags.index'));
    }
}

