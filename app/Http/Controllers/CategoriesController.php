<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact(['categories']));
    }
    public function create()
    {
         return view('admin.categories.create');
    }
    public function store(Request $request)
    {
         try{
           Category::create(['name' => $request->name]);
           return redirect(route('categories.index'))
                ->with(['status' => 'success',
                    'message' => 'Category Added Successfully!'
                ]);

         }catch(\Exception $e){
            return redirect(route('categories.index'))
            ->with(['status' => 'danger',
                'message' => 'OOPS! Something went wrong!'
            ]);

         }
    }
    public function show(Category $category)
    {

    }
    public function edit(Category $category)
    {
        return view('admin.categories.edit', compact(['category']));
    }
    public function update(Request $request, Category $category)
    {
//        $category->name = $request->name;
//        $category->save();
        $category->update(['name'=>$request->name]);
        return redirect(route('categories.index'))->with(
            [
                'status' => 'success',
                'message' => 'Category Updated Successfully!'
            ]
        );
    }



    public function destroy(Category $category)
    {
        if($category->posts()->count()>0){
            return redirect(route('categories.index'))->with(
                [
                    'status' => 'danger',
                    'message' => 'Category Has Posts! Cannot Delete'
                ]
            );

         }
        $category->delete();
        return redirect(route('categories.index'))->with(
            [
                'status' => 'success',
                'message' => 'Category Has been deleted!'
            ]
        );

    }
}

