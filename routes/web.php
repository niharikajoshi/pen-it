<?php

use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\TagsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/blogs/tags/{tag}', [FrontendController::class, 'tags'])->name('tag.blogs');
Route::get('/blogs/category/{category}', [FrontendController::class, 'category'])->name('category.blogs');

/**
 * Admin Panel Routes
 */

Route::middleware(['auth'])->group(function() {
    Route::resource('categories', CategoriesController::class);
    Route::resource('tags', TagsController::class);
    Route::resource('posts', PostsController::class);
    Route::get('/trashed/posts', [PostsController::class, 'trashed'])->name('posts.trashed');

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');
});
Route::middleware(['auth', 'admin'])->group(function() {
    Route::get('/users', [UsersController::class, 'index'])->name('users.index');
    Route::put('/users/{user}/make-admin', [UsersController::class, 'makeAdmin'])->name('users.makeAdmin');
});



require __DIR__.'/auth.php';

