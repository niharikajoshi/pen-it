<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('user_id');
            $table->string('title');
            $table->string('excerpt');
            $table->text('body');
            $table->string('image');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();//jabhi bhi delete ho koi posts toh usse permanently delete nahi krenge, soft delete krenge, incase to recover , soft delete will add a deleted_at table which will store the timestamp of the deleted post i.e deleted at ... and hence only that posts which be shown in frontend jinka deleted_at ka column empty ho, jaise hi koi posts maine delete ki uska time deleted_at mei daalega and uske baad woh post database mei rahegi bas frontend mei nahi dikhegi...Post ke model mei bhi softdelete ko use krenge.
            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories')
                  ->onDelete('cascade');

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
};
