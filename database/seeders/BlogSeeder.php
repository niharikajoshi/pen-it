<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryNews = Category::create(['name'=> 'News']); //yahan category likha toh yaad aaya ki protected fillable methods daalne hai
        $categoryDesign = Category::create(['name'=> 'Design']);
        $categoryTechnology = Category::create(['name'=> 'Technology']);
        $categoryEngineering = Category::create(['name'=> 'Engineering']);

        $tagCustomer = Tag::create(['name'=>'Customer']);
        $tagCoding = Tag::create(['name'=>'Coding']);
        $tagFrontend = Tag::create(['name'=>'Frontend']);
        $tagBackend = Tag::create(['name'=>'Backend']);
        $tagJava = Tag::create(['name'=>'Java']);

        $post1 = Post::create([
            'category_id'=> $categoryDesign->id,
            'user_id'=> User::all()->random()->id,
            'title'=> fake()->sentence,
            //'title' => fake()->sentence, for version 9 of laravel fake() method is defined
            'excerpt' => fake()->sentence,
            'body' => fake()->paragraph(10),
            'image' => 'posts-images/1.jpg',
            'published_at' => Carbon::now()->format('y-m-d')

        ]);
        $post2 = Post::create([
            'category_id'=> $categoryTechnology->id,
            'user_id'=> User::all()->random()->id,
            'title'=> fake()->sentence,
            //'title' => fake()->sentence, for version 9 of laravel fake() method is defined
            'excerpt' => fake()->sentence,
            'body' => fake()->paragraph(10),
            'image' => 'posts-images/2.jpg',
            'published_at' => Carbon::now()->format('y-m-d')

        ]);
        $post3 = Post::create([
            'category_id'=> $categoryEngineering->id,
            'user_id'=> User::all()->random()->id,
            'title'=> fake()->sentence,
            //'title' => fake()->sentence, for version 9 of laravel fake() method is defined
            'excerpt' => fake()->sentence,
            'body' => fake()->paragraph(10),
            'image' => 'posts-images/3.jpg',
            'published_at' => Carbon::now()->format('y-m-d')

        ]);

        //yahan pe bridging table ka data daala hai, tags for particular post id
        $post1->tags()->attach([$tagCustomer->id, $tagFrontend->id]); //used attach function here for attaching id
        $post2->tags()->attach([$tagCoding->id, $tagFrontend->id, $tagFrontend->id]);
        $post3->tags()->attach([$tagCustomer->id, $tagBackend->id, $tagJava->id, $tagCoding->id]);//used attach function here for attaching id, attach function use krne ke liye Relationships daalna padega model mei


    }
}
